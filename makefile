tool: source/gstool/main.c
	clang -g -O1 -Wall -Wpedantic -Wno-nullability-extension -Wno-nullability-completeness -fsanitize=address,undefined -o tool source/gstool/main.c -lreadline

clean: 
	rm -rf tool.dysm
	rm tool