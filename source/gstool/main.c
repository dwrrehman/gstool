//
//  main.c
//  gstool
//
//  Created by Daniel Rehman on 2010117.
//

///
///  gstool:
///          a utility to help daniel organize information about
///          candiate graduate schools he will be applying to.
///
///          note:   active columns that are usually used:
///
///                0 1 4 5 6 7 12 13 14 15 16 17 18
///

#include <iso646.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include <dirent.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>

#include <readline/readline.h>
#include <readline/history.h>

/// --------------------------- parameters ----------------------------

const char* schools_directory = "/Users/deniylreimn/Documents/school/schools/";
const char* prompt_emblem = ":> ";

/// -------------------------------------------------------------------

typedef size_t nat;

struct termios terminal = {0};

enum column_indexes {
    abbreviation,
    name,
    website,
    phone,
    location,
    application_deadline,
    requires_gre,
    research_focused,
    has_health_insurance,
    has_assistantship,
    has_housing,
    application_submitted,
    years_for_completion,
    application_fee,
    gre_school_code,
    gre_department_code,
    letters_sent,
    financial_aid_score,
    overall_score,
    notes,
    
    total_column_count
};

const char* column_names[] = {
    "abbreviation",
    "name",
    "website",
    "phone",
    "location",
    "application_deadline",
    "requires_gre",
    "research_focused",
    "has_health_insurance",
    "has_assistantship",
    "has_housing",
    "application_submitted",
    "years_for_completion",
    "application_fee",
    "gre_school_code",
    "gre_department_code",
    "letters_sent",
    "financial_aid_score",
    "overall_score",
    "notes",
};

const char* table_column_names[] = {
    "school",
    "name",
    "website",
    "phone",
    "location",
    "deadline",
    "gre?",
    "research?",
    "insurance?",
    "ta?",
    "housing?",
    "submitted?",
    "duration",
    "fee",
    "school code",
    "dep. code",
    "letters sent",
    "FA score",
    "overall",
    "notes",
};

static const char *clear_screen = "\033[1;1H\033[2J";

struct school {
    bool requires_gre;
    bool research_focused;
    bool has_health_insurance;
    bool has_assistantship;
    bool has_housing;
    bool application_submitted;
    
    nat overall_score;
    nat financial_aid_score;
    nat years_for_completion;
    nat application_fee;
    nat gre_school_code;
    nat gre_department_code;
    nat letters_sent;
    
    char abbreviation[10];
    char name[1024];
    char website[1024];
    char phone[1024];
    char location[1024];
    char application_deadline[1024];
    char notes[4096];
};

/// ----------------- helper functions: --------------------

static inline const char* boolean_to_string(bool x) {
    return x ? "true" : "false";
}

static inline bool strings_equal(const char* a, const char* b) {
    return !strcmp(a, b);
}

static inline bool strings_equal_abbrev(const char* a, const char* b, const char* b_short) {
    return !strcmp(a, b) || !strcmp(a, b_short);
}

///  modifies input string.
static inline char** segment(char* string, nat* count) {
    char** words = NULL;
    nat word_count = 0;
    char* word = strtok(string, " ");
    while (word) {
        words = realloc(words, sizeof(char*) * (word_count + 1));
        words[word_count++] = word;
        word = strtok(NULL, " ");
    }
    *count = word_count;
    return words;
}

static inline void read_string(char* output, char** words, nat count) {
    strcpy(output, "");
    for (int i = 0; i < count; i++) {
        strcat(output, words[i]);
        if (i != count - 1) strcat(output, " ");
    }
}

static inline void read_number(nat* output, char* line) {
    *output = atoi(line);
}

static inline void read_boolean(bool* output, char* line) {
    if (strings_equal_abbrev(line, "false", "0")) *output = false;
    else if (strings_equal_abbrev(line, "true", "1")) *output = true;
}


/// ------------------- IO functions -----------------------------

void restore_terminal() {
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &terminal) < 0) {
        perror("tcsetattr(STDIN_FILENO, TCSAFLUSH, &terminal))");
    }
}

void configure_terminal() {
    if (tcgetattr(STDIN_FILENO, &terminal) < 0) {
        perror("tcgetattr(STDIN_FILENO, &terminal)");
    }
    
    atexit(restore_terminal);
    struct termios raw = terminal;
    raw.c_lflag &= ~(ECHO | ICANON);
    
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) < 0) {
        perror("tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw)");
    }
}

static inline char getch() {
    char c = 0;
    const ssize_t n = read(STDIN_FILENO, &c, 1);
    if (n < 0) {
        printf("n < 0 : ");
        perror("read(STDIN_FILENO, &c, 1) syscall");
                
    } else if (n == 0) {
        printf("n == 0 : ");
        perror("read(STDIN_FILENO, &c, 1) syscall");
    }
    return c;
}

static inline void prompt(const char* prompt_message, char* buffer, int size) {
                
    printf("\n\n\n\t\t %s", prompt_message);
    printf("\n\n\n%s", prompt_emblem);
    fflush(stdout);
    
    fgets(buffer, size, stdin);
    buffer[strlen(buffer) - 1] = '\0';
}

static inline char* open_file(char *path, nat* length) {
    
    FILE* file = fopen(path, "r");
    
    if (!file) {
        perror("load_schools: fopen");
        return NULL;
    }
    
    fseek(file, 0, SEEK_END);
    *length = ftell(file);
    fseek(file, 0, SEEK_SET);
    
    char* contents = calloc(*length + 1, sizeof(char));
    fread(contents, sizeof(char), *length, file);
    fclose(file);
    
    return contents;
}

/// -------------------------------------------------------------------

static inline struct school parse_school_file(char* filename, char* contents, nat length) {
    
    struct school school = {0};
    
    for (char* next = NULL, * line = contents; line; line = next ? (next + 1) : NULL) {
        
        next = strchr(line, '\n');
        if (next)
            *next = '\0';
        
        if (!strlen(line) || line[0] == '#') {
            line = next ? (next + 1) : NULL;
            continue;
        }
                        
        nat count = 0;
        char** words = segment(line, &count);
        
             if (strings_equal(words[0], column_names[abbreviation]))          read_string(school.abbreviation, words + 2, count - 2);
        else if (strings_equal(words[0], column_names[name]))                  read_string(school.name, words + 2, count - 2);
        else if (strings_equal(words[0], column_names[location]))              read_string(school.location, words + 2, count - 2);
        else if (strings_equal(words[0], column_names[website]))               read_string(school.website, words + 2, count - 2);
        else if (strings_equal(words[0], column_names[phone]))                 read_string(school.phone, words + 2, count - 2);
        else if (strings_equal(words[0], column_names[application_deadline]))  read_string(school.application_deadline, words + 2, count - 2);
        
        else if (strings_equal(words[0], column_names[overall_score]))         read_number(&school.overall_score, words[2]);
        else if (strings_equal(words[0], column_names[financial_aid_score]))   read_number(&school.financial_aid_score, words[2]);
        else if (strings_equal(words[0], column_names[gre_school_code]))       read_number(&school.gre_school_code, words[2]);
        else if (strings_equal(words[0], column_names[gre_department_code]))   read_number(&school.gre_department_code, words[2]);
        else if (strings_equal(words[0], column_names[application_fee]))       read_number(&school.application_fee, words[2]);
        else if (strings_equal(words[0], column_names[years_for_completion]))  read_number(&school.years_for_completion, words[2]);
        else if (strings_equal(words[0], column_names[letters_sent]))          read_number(&school.letters_sent, words[2]);
                
        else if (strings_equal(words[0], column_names[requires_gre]))          read_boolean(&school.requires_gre, words[2]);
        else if (strings_equal(words[0], column_names[research_focused]))      read_boolean(&school.research_focused,words[2]);
        else if (strings_equal(words[0], column_names[has_health_insurance]))  read_boolean(&school.has_health_insurance, words[2]);
        else if (strings_equal(words[0], column_names[has_assistantship]))     read_boolean(&school.has_assistantship, words[2]);
        else if (strings_equal(words[0], column_names[has_housing]))           read_boolean(&school.has_housing, words[2]);
        else if (strings_equal(words[0], column_names[application_submitted])) read_boolean(&school.application_submitted, words[2]);
        
        else if (strings_equal(words[0], column_names[notes])) {
            strcpy(school.notes, next + 1);
            break;
        } else {
            printf("parse_school_file: %s: error: unknown school attribute: %s\n", filename, words[0]);
            return (struct school){0};
        }
    }
        
    return school;
}

static inline struct school load_school(char* school_filename) {
    
    char path[4096] = {0};
    strcpy(path, schools_directory);
    strcat(path, school_filename);
    
    printf("loading \"%s\"...\n", school_filename);
    
    nat length = 0;
    char* contents = open_file(path, &length);
    if (!contents) return (struct school) {0};
    
    struct school school = parse_school_file(school_filename, contents, length);
    free(contents);
    
    return school;
}

static inline struct school* load_schools(nat* count) {
        
    printf("loading schools...\n");
    fflush(stdout);
    
    struct school* schools = NULL;
    nat school_count = 0;
    
    DIR* dir = opendir(schools_directory);
    if (!dir) {
        perror("load_school: opendir");
        return NULL;
    }

    for (struct dirent* entry = readdir(dir); entry; entry = readdir(dir)) {
        
        if (strings_equal(".", entry->d_name) || strings_equal("..", entry->d_name))
            continue;
                        
        struct school school = load_school(entry->d_name);
        if (!strlen(school.abbreviation)) continue;
        
        schools = realloc(schools, sizeof(struct school) * (school_count + 1));
        schools[school_count++] = school;
    }
    
    printf("loaded %lu schools.\n", school_count);
    *count = school_count;
    return schools;
}

static inline void debug_school(struct school s) {
    printf("\t abbreviation = %s\n", s.abbreviation);
    printf("\t name = %s\n", s.name);
    printf("\t location = %s\n", s.location);
    
    printf("\t application_deadline = %s\n", s.application_deadline);
    printf("\t application_fee = %lu\n", s.application_fee);
    
    printf("\t application_submitted = %s\n", boolean_to_string(s.application_submitted));
    printf("\t letters_sent = %lu\n", s.letters_sent);
    
    printf("\t overall_score = %lu\n", s.overall_score);
    printf("\t financial_aid_score = %lu\n", s.financial_aid_score);
    
    printf("\t requires_gre = %s\n", boolean_to_string(s.requires_gre));
    printf("\t research_focused = %s\n", boolean_to_string(s.research_focused));
            
    printf("\t years_for_completion = %lu\n", s.years_for_completion);
    printf("\t has_health_insurance = %s\n", boolean_to_string(s.has_health_insurance));
    printf("\t has_assistantship = %s\n", boolean_to_string(s.has_assistantship));
    printf("\t has_housing = %s\n", boolean_to_string(s.has_housing));
            
    printf("\t gre_school_code = %lu\n", s.gre_school_code);
    printf("\t gre_department_code = %lu\n", s.gre_department_code);
    
    printf("\t website = %s\n", s.website);
    printf("\t phone = %s\n", s.phone);
            
    printf("\t notes : \n%s\n", s.notes);
}

static inline void debug_schools(struct school* schools, nat school_count) {
    for (nat i = 0; i < school_count; i++) {
        printf("# ------- SCHOOL # %lu---------\n\n", i);
        debug_school(schools[i]);
        
        printf("[press return to advance]");
        getch();
    }
}

static inline void generate_school_file_for(struct school s, const char* filename) {
            
    FILE* file = fopen(filename, "w");
    if (!file) {
        printf("error writing file: fopen: %s\n", strerror(errno));
        return;
    }
    
    printf("generating file \"%s\"...\n", filename);
    
    fprintf(file, "abbreviation = %s\n", s.abbreviation);
    fprintf(file, "name = %s\n", s.name);
    fprintf(file, "location = %s\n\n", s.location);
    
    fprintf(file, "application_deadline = %s\n", s.application_deadline);
    fprintf(file, "application_fee = %lu\n\n", s.application_fee);
    
    fprintf(file, "application_submitted = %s\n", boolean_to_string(s.application_submitted));
    fprintf(file, "letters_sent = %lu\n\n", s.letters_sent);
    
    fprintf(file, "overall_score = %lu\n", s.overall_score);
    fprintf(file, "financial_aid_score = %lu\n\n", s.financial_aid_score);
    
    fprintf(file, "requires_gre = %s\n", boolean_to_string(s.requires_gre));
    fprintf(file, "research_focused = %s\n\n", boolean_to_string(s.research_focused));
    
    fprintf(file, "years_for_completion = %lu\n", s.years_for_completion);
    fprintf(file, "has_health_insurance = %s\n", boolean_to_string(s.has_health_insurance));
    fprintf(file, "has_assistantship = %s\n", boolean_to_string(s.has_assistantship));
    fprintf(file, "has_housing = %s\n\n", boolean_to_string(s.has_housing));
    
    fprintf(file, "gre_school_code = %lu\n", s.gre_school_code);
    fprintf(file, "gre_department_code = %lu\n\n", s.gre_department_code);
    
    fprintf(file, "website = %s\n", s.website);
    fprintf(file, "phone = %s\n\n", s.phone);
    fprintf(file, "notes : \n%s\n\n", s.notes);
    fclose(file);
    
    printf("file generated.\n");
}

/// note: cannot set notes attribute, as it's a multiline string.
static inline void set_attribute(nat attribute, char* line, struct school* school) {
    nat count = 0;
    char** words = segment(line, &count);
    
    if (!count) return;
    
         if (attribute == abbreviation)           read_string(school->abbreviation, words, count);
    else if (attribute == name)                   read_string(school->name, words, count);
    else if (attribute == location)               read_string(school->location, words, count);
    else if (attribute == website)                read_string(school->website, words, count);
    else if (attribute == phone)                  read_string(school->phone, words, count);
    else if (attribute == application_deadline)   read_string(school->application_deadline, words, count);
    
    else if (attribute == overall_score)          read_number(&school->overall_score, words[0]);
    else if (attribute == financial_aid_score)    read_number(&school->financial_aid_score, words[0]);
    else if (attribute == gre_school_code)        read_number(&school->gre_school_code, words[0]);
    else if (attribute == gre_department_code)    read_number(&school->gre_department_code, words[0]);
    else if (attribute == application_fee)        read_number(&school->application_fee, words[0]);
    else if (attribute == years_for_completion)   read_number(&school->years_for_completion, words[0]);
    else if (attribute == letters_sent)           read_number(&school->letters_sent, words[0]);

    else if (attribute == requires_gre)           read_boolean(&school->requires_gre, words[0]);
    else if (attribute == research_focused)       read_boolean(&school->research_focused, words[0]);
    else if (attribute == has_health_insurance)   read_boolean(&school->has_health_insurance, words[0]);
    else if (attribute == has_assistantship)      read_boolean(&school->has_assistantship, words[0]);
    else if (attribute == has_housing)            read_boolean(&school->has_housing, words[0]);
    else if (attribute == application_submitted)  read_boolean(&school->application_submitted, words[0]);
    
    else if (attribute == notes) {
        printf("set_attribute: error: cannot set notes attribute.\n");
    } else {
        printf("set attribute aborted.\n");
    }
}

static inline void prompt_and_set_attribute(nat i, struct school *school) {
            
    char line[4096] = {0};
    
    if (i == notes) {
        printf("\n\n\n\t\t%s", column_names[i]);
        printf("\n\n\n: ");
        fflush(stdout);
        while (true) {
            fgets(line, sizeof line, stdin);
            if (strings_equal(line, ".\n")) break;
            strcat(school->notes, line);
        }
        
    } else {
        prompt(column_names[i], line, sizeof line);
        set_attribute(i, line, school);
    }
}

static inline void add_new_school(struct school* schools, nat* school_count) {
                                    
    char filename[4096] = {0};
    prompt("filename", filename, sizeof filename);
    
    struct school school = {0};
    for (nat i = 0; i < total_column_count; i++)
        prompt_and_set_attribute(i, &school);
            
    char path[4096] = {0};
    strcpy(path, schools_directory);
    strcat(path, filename);
    generate_school_file_for(school, path);
}

static inline void unload_all_schools(struct school** schools, nat* school_count) {
    free(*schools);
    *schools = NULL;
    *school_count = 0;
}

static inline void print_column_list() {
    printf("columns indicies:\n");
    for (nat i = 0; i < total_column_count; i++)
        printf("\t\t%5lu  :  %s \n", i, column_names[i]);
}

static inline void print_active_columns(nat* active_columns, nat count) {
    printf("\ncolumns indicies: {");
    for (nat i = 0; i < count; i++)
        printf("%lu ", active_columns[i]);
    printf("}\n\n");
    
    printf("columns: {");
    for (nat i = 0; i < count; i++)
        printf("%s,  ", column_names[active_columns[i]]);
    printf("}\n\n");
}

static inline void edit_attribute_for_school(char* abbrev) {
        
    char filename[4096] = {0};
    strcpy(filename, abbrev);
    strcat(filename, ".txt");
    
    struct school school = load_school(filename);
    if (!strlen(school.abbreviation)) return;
            
    for (nat i = 0; i < total_column_count; i++)
        printf("\t\t%5lu  :  %s \n", i, column_names[i]);
                
    char number[128] = {0};
    prompt("attribute number", number, sizeof number);
    prompt_and_set_attribute(atoi(number), &school);
    
    char path[4096] = {0};
    strcpy(path, schools_directory);
    strcat(path, filename);
    
    generate_school_file_for(school, path);
}

static inline void reload_all_schools(nat *school_count, struct school** schools) {
    printf("reloading all schools...\n");
    fflush(stdout);
    unload_all_schools(schools, school_count);
    *schools = load_schools(school_count);
}

static inline void print_help() {
    printf(
           "available commands: \n\n"
           
           "\t quit(q) : quits the command line utility.\n"
           "\t clear(l) : clears the screen.\n"
           "\t help(h) : this menu.\n"
           
           "\t table(t) : display the current columns in a table of schools.\n"
           "\t debug : list the raw data for each school. used for debugging.\n"
           "\t reload(r) : reload the internal list of schools to match the directory contents.\n"
           
           "\t add(a) : add a new school to the list, and generate a file for it.\n"
           "\t edit : edit an attribute of a particular school.\n"
           "\t column(c) : add a new column to the active column list, for the table.\n"
           
           "\t empty : remove all active columns in the table.\n"
           "\t full : add in all active columns in the table.\n"
           "\t remove : remove a particular active column in the table.\n"
           "\t set columns : set the active columns from a given string of space seperated nats.\n"
          
           "\t list all: list all posssible columns and their indicies, for reference. \n"
           "\t list : list the current active columns, used in the table, names and indices. \n"
           
           "\t sort : sort the list of schools, by a particular column.\n"
                                 
           "\n");
}

static inline int print_attribute_into_buffer(const nat attribute, char* buffer, const struct school* this, const int width) {
        
    int p = 0;
    
    if (attribute == abbreviation)                p += sprintf(buffer, "%*s", width, this->abbreviation);
    if (attribute == name)                        p += sprintf(buffer, "%*s", width, this->name);
    else if (attribute == location)               p += sprintf(buffer, "%*s", width, this->location);
    else if (attribute == website)                p += sprintf(buffer, "%*s", width, this->website);
    else if (attribute == phone)                  p += sprintf(buffer, "%*s", width, this->phone);
    else if (attribute == application_deadline)   p += sprintf(buffer, "%*s", width, this->application_deadline);
    
    else if (attribute == overall_score)          p += sprintf(buffer, "%*lu", width, this->overall_score);
    else if (attribute == financial_aid_score)    p += sprintf(buffer, "%*lu", width, this->financial_aid_score);
    else if (attribute == gre_school_code)        p += sprintf(buffer, "%*lu", width, this->gre_school_code);
    else if (attribute == gre_department_code)    p += sprintf(buffer, "%*lu", width, this->gre_department_code);
    else if (attribute == application_fee)        p += sprintf(buffer, "%*lu", width, this->application_fee);
    else if (attribute == years_for_completion)   p += sprintf(buffer, "%*lu", width, this->years_for_completion);
    else if (attribute == letters_sent)           p += sprintf(buffer, "%*lu", width, this->letters_sent);
    
    else if (attribute == requires_gre)           p += sprintf(buffer, "%*s", width, this->requires_gre ? " x " : "   ");
    else if (attribute == research_focused)       p += sprintf(buffer, "%*s", width, this->research_focused ? " x " : "   ");
    else if (attribute == has_health_insurance)   p += sprintf(buffer, "%*s", width, this->has_health_insurance ? " x " : "   ");
    else if (attribute == has_assistantship)      p += sprintf(buffer, "%*s", width, this->has_assistantship ? " x " : "   ");
    else if (attribute == has_housing)            p += sprintf(buffer, "%*s", width, this->has_housing ? " x " : "   ");
    else if (attribute == application_submitted)  p += sprintf(buffer, "%*s", width, this->application_submitted ? " x " : "   ");
    
    return p;
}

static inline void determine_maximum_column_widths(nat active_column_count, nat* active_columns,
                                                   nat school_count, struct school* schools,
                                                   int* widths) {
    char buffer[4096] = {0};
    for (nat c = 0; c < active_column_count; c++) {
        
        const nat attribute = active_columns[c];
        memset(buffer, 0, sizeof buffer);
        int p = sprintf(buffer, "%s", table_column_names[attribute]);
        if (p > widths[c]) widths[c] = p;
        
        for (nat s = 0; s < school_count; s++) {
            struct school this = schools[s];
            
            memset(buffer, 0, sizeof buffer);
            int p = 0;
            print_attribute_into_buffer(attribute, buffer, &this, 0);
            p = (int) strlen(buffer);
            if (p > widths[c]) widths[c] = p;
        }
    }
}

static inline void display_table(nat* active_columns, nat active_column_count,
                                 struct school* schools, nat school_count) {
    int widths[active_column_count];
    memset(widths, 0, sizeof(int) * active_column_count);
    determine_maximum_column_widths(active_column_count, active_columns, school_count, schools, widths);
            
    printf("┌");
    for (nat c = 0; c < active_column_count; c++) {
        for (nat i = 0; i < widths[c] + 2; i++) printf("─");
        if (c == active_column_count - 1) printf("┐"); else printf("┬");
    }
    puts("");
    
    printf("│");
    for (nat c = 0; c < active_column_count; c++)
        printf(" %*s │", widths[c], table_column_names[active_columns[c]]);
    puts("");
        
    printf("╞");
    for (nat c = 0; c < active_column_count; c++) {
        for (nat i = 0; i < widths[c] + 2; i++) printf("═");
        if (c == active_column_count - 1) printf("╡"); else printf("╪");
    }
    puts("");
        
    for (nat s = 0; s < school_count; s++) {
        
        struct school this = schools[s];
        
        printf("│");
        for (nat c = 0; c < active_column_count; c++) {
            char buffer[4096] = {0};
            print_attribute_into_buffer(active_columns[c], buffer, &this, widths[c]);
            printf(" %s │", buffer);
        }
        puts("");
                
        if (s == school_count - 1) {
            printf("└");
            for (nat c = 0; c < active_column_count; c++) {
                for (nat i = 0; i < widths[c] + 2; i++) printf("─");
                if (c == active_column_count - 1) printf("┘"); else printf("┴");
            }
        } else {
            printf("├");
            for (nat c = 0; c < active_column_count; c++) {
                for (nat i = 0; i < widths[c] + 2; i++) printf("─");
                if (c == active_column_count - 1) printf("┤"); else printf("┼");
            }
        }
        puts("");
    }
}

static inline void add_column(nat** active_columns,  nat * active_column_count) {

    for (nat i = 0; i < total_column_count; i++)
        printf("\t\t%5lu  :  %s \n", i, column_names[i]);
                
    char number[128] = {0};
    prompt("column number to add", number, sizeof number);
    
    const nat n = atoi(number);
    *active_columns = realloc(*active_columns, sizeof(nat) * (*active_column_count + 1));
    (*active_columns)[(*active_column_count)++] = n;
    printf("added %lu to the columns.\n", n);
}

static inline void empty_all_columns(nat** active_columns,  nat* active_column_count) {
    free(*active_columns);
    *active_columns = NULL;
    *active_column_count = 0;
    printf("removed all active columns.\n");
}

static inline void remove_column(nat** active_columns, nat* active_column_count) {
        
    for (nat i = 0; i < *active_column_count; i++)
        printf("\t\t%5lu  :  %s \n", i, column_names[(*active_columns)[i]]);
                
    char number[128] = {0};
    prompt("column number to remove", number, sizeof number);
    
    const nat n = atoi(number);
    memmove(*active_columns + n, *active_columns + n + 1, sizeof(nat) * (*active_column_count - (n + 1)));
    *active_columns = realloc(*active_columns, sizeof(nat) * (--*active_column_count));
    printf("removed index [%lu] from the columns.\n", n);
}

static inline void add_all_columns(nat** active_columns, nat* active_column_count) {
    empty_all_columns(active_columns, active_column_count);
    for (nat c = 0; c < total_column_count - 1; c++) {
        *active_columns = realloc(*active_columns, sizeof(nat) * (*active_column_count + 1));
        (*active_columns)[(*active_column_count)++] = c;
    }
    printf("added all columns to the table\n");
}

static inline void set_columns(nat** active_columns, nat* active_column_count) {
    
    empty_all_columns(active_columns, active_column_count);
    
    nat count = 0;
    char buffer[4096] = {0};
    prompt("active columns", buffer, sizeof buffer);
    char** numbers = segment(buffer, &count);
    
    *active_columns = malloc(sizeof(nat) * count);
    for (nat i = 0; i < count; i++)
        (*active_columns)[i] = atoi(numbers[i]);
    *active_column_count = count;
    
    printf("set %lu active columns.\n", count);
}




int overall_score_comparison(const void* _a, const void* _b) {
    struct school a = *(struct school*)_a;
    struct school b = *(struct school*)_b;
    return a.overall_score > b.overall_score;
}

int financial_aid_score_comparison(const void* _a, const void* _b) {
    struct school a = *(struct school*)_a;
    struct school b = *(struct school*)_b;
    return a.financial_aid_score > b.financial_aid_score;
}

int abbreviation_comparison(const void* _a, const void* _b) {
    struct school a = *(struct school*)_a;
    struct school b = *(struct school*)_b;
    return strcmp(a.abbreviation, b.abbreviation);
}

static inline void sort_schools(struct school* schools, nat school_count) {
    
    for (nat i = 0; i < total_column_count; i++)
        printf("\t\t%5lu  :  %s \n", i, column_names[i]);
                
    char number[128] = {0};
    prompt("column number to sort by", number, sizeof number);
    
    const nat n = atoi(number);
    
    if (n == abbreviation) qsort(schools, school_count, sizeof(struct school), abbreviation_comparison);
    else if (n == overall_score) qsort(schools, school_count, sizeof(struct school), overall_score_comparison);
    else if (n == financial_aid_score) qsort(schools, school_count, sizeof(struct school), overall_score_comparison);
    
    else {
        printf("error: sorting function unimplemented for that attribute.\n");
    }
}


int main() {
    using_history();
    
    nat school_count = 0;
    struct school* schools = load_schools(&school_count);
    //if (!schools) exit(1);
    
    nat active_column_count = 10;
    nat* active_columns = malloc(sizeof(nat) * 10);
        
    nat init[] = {0, 1, 4, 5, 6, 7, 12, 13, 17, 18};
    for (int i = 0; i < 10; i++) active_columns[i] = init[i];
        
    while (true) {
        
        char* input = readline(prompt_emblem);
        add_history(input);
                
        nat word_count = 0;
        char** words = segment(input, &word_count);
        if (!word_count or !words or !words[0]) continue;
        char* first = words[0];
        char* second = word_count == 2 ? words[1] : NULL;
        
        if (strings_equal(first, ""))
            continue;
        
        else if (word_count == 1 and
                 strings_equal_abbrev(first, "quit", "q"))
            break;
        
        else if (word_count == 1 and
                 strings_equal_abbrev(first, "clear", "l"))
            printf("%s", clear_screen);
        
        else if (word_count == 1 and
                 strings_equal_abbrev(first, "help", "h"))
            print_help();
        
        else if (word_count == 1 and
                 strings_equal_abbrev(first, "table", "t"))
            display_table(active_columns, active_column_count, schools, school_count);
        
        else if (word_count == 1 and
                 strings_equal(first, "debug"))
            debug_schools(schools, school_count);
        
        else if (word_count == 1 and
                 strings_equal_abbrev(first, "reload", "r"))
            reload_all_schools(&school_count, &schools);
        
        else if (word_count == 1 and
                 strings_equal_abbrev(first, "add", "a"))
            add_new_school(schools, &school_count);
        
        else if (word_count == 2 and
                 strings_equal(first, "edit"))
            edit_attribute_for_school(second);
        
        else if (word_count == 1 and
                 strings_equal_abbrev(first, "column", "c"))
            add_column(&active_columns, &active_column_count);
        
        else if (strings_equal(first, "empty"))
            empty_all_columns(&active_columns, &active_column_count);
        
        else if (strings_equal(first, "full"))
            add_all_columns(&active_columns, &active_column_count);
        
        else if (strings_equal(first, "remove"))
            remove_column(&active_columns, &active_column_count);
    
        else if (word_count == 2 and
                 strings_equal(first, "set") and
                 strings_equal(second, "columns"))
            set_columns(&active_columns, &active_column_count);

        else if (word_count == 2 and
                 strings_equal(first, "list") and
                 strings_equal(second, "all"))
            print_column_list();

        else if (word_count == 1 and
                 strings_equal(first, "list"))
            print_active_columns(active_columns, active_column_count);
        
        else if (word_count == 1 and
                 strings_equal(first, "sort"))
            sort_schools(schools, school_count);
        
        else
            printf("error: unknown command: '%s'\n", first);
                
        free(input);
    }
    clear_history();
}
